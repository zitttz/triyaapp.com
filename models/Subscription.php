<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property integer $subscriber
 * @property integer $author
 *
 * @property Users $author0
 * @property Users $subscriber0
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriber', 'author'], 'required'],
            [['subscriber', 'author'], 'integer'],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['author' => 'id']],
            [['subscriber'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['subscriber' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscriber' => 'Subscriber',
            'author' => 'Author',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(Users::className(), ['id' => 'author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber0()
    {
        return $this->hasOne(Users::className(), ['id' => 'subscriber']);
    }

    public function setSubscription($authorId, $subscriberId){
        $this->author = $authorId;
        $this->subscriber = $subscriberId;
        if ($this->save()){
            return true;
        }else{
            return false;
        }
    }
}
