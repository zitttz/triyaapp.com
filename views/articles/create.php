<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Articles */

$this->title = 'Создание статьи';
$this->params['breadcrumbs'][] = ['label' => 'Мои статьи', 'url' => ['index?ArticlesSearcha%5Bauthor%5D=1']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
