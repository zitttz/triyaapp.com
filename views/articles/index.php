<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticlesSearcha */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Все cтатьи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Articles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    foreach ($articles as $article){
        echo $this->render('_view', [
            'article' => $article,
        ]);
    }
    ?>
</div>
