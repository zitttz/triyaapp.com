<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Services */

?>
<div class="acrtile-view-<?=Html::encode($article['id'])?>">

    <div class="row">
        <h4 class="pull-left"><?= Html::encode($article['title']) ?></h4>

        <div class="text-right">
            <?= 'Автор: '.Html::encode($article['username']) ?>
        </div>
    </div>
    <p>
        <?= substr($article['text'], 0, 200).'...'.Html::a('прочитать', ['articles/view/'.$article['id']], ['class' => '']) ?>
    </p>
    <hr>

</div>
