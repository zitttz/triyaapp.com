<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Articles */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-view">

    <div class="row">
        <h1><?= Html::encode($model->title) ?></h1>
    </div>
    <div class="row">
        <?php if(!$isMaster && !$isSubsriber):?>
        <h4 class="pull-right"> <?= Html::a('Подписаться', ['subscription?id='.$author->id.'&article='.$model->id], ['class' => 'btn btn-success']) ?></h4>
        <?php elseif($isSubsriber): ?>
        <h4 class="pull-right"> <?= Html::a('Отписаться', ['unsubscription?id='.$author->id.'&article='.$model->id], ['class' => 'btn btn-danger']) ?></h4>
        <?php endif; ?>

        <h4 class="pull-left">Статья создана: <?= $model->created_at?><br>Автор: <?= $author->username ?> </h4>


    </div>
    <p>
        <?php
        if ($isMaster) {
            echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ;
        } ?>
    </p>

    <p>
        <?= Html::encode($model->text) ?>
    </p>


</div>
