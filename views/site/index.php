<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать!</h1>

        <p class="lead">Это тестовое задание, для начала работы зайдите в систему или авторизируйтесь.</p>

        <p>
            <?= \yii\helpers\Html::a('Войти в систему', ['site/login'], ['class' => 'btn btn-lg btn-success']) ?>
        </p>
    </div>

</div>
