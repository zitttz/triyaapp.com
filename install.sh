#!/bin/bash
php yii migrate/up --interactive=0
php yii users/create
php yii fixture/load Articles --namespace='app\fixtures' --interactive=0
php yii fixture/load Subscription --namespace='app\fixtures' --interactive=0
