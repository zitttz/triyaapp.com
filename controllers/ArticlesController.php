<?php

namespace app\controllers;

use app\models\Subscription;
use app\models\Users;
use Yii;
use app\models\Articles;
use app\models\ArticlesSearcha;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = new Query();
        $provider = new ArrayDataProvider([
            'allModels' => $query->select('articles.id, title, text, users.id as author, users.username')->from('articles')->leftJoin('users','users.id = articles.author')->all(),
            'sort' => [
                'attributes' => [
                    'id',
                    'created_at' => [
                        'asc' => ['created_at' => SORT_ASC],
                        'desc' => ['created_at' => SORT_DESC],
                        'default' => SORT_ASC,
                        'label' => 'Время создания',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $articles = $provider->getModels();

        return $this->render('index', [
            'articles' => $articles,
        ]);
    }

    public function actionMy()
    {
        $query = new Query();
        $provider = new ArrayDataProvider([
            'allModels' => $query->select('articles.id, title, text, users.id as author, users.username')->from('articles')->where('articles.author = :author',[':author' => Yii::$app->user->identity->id])->leftJoin('users','users.id = articles.author')->all(),
            'sort' => [
                'attributes' => ['id', 'created_at'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $articles = $provider->getModels();

        return $this->render('my', [
            'articles' => $articles,
        ]);
    }


    public function actionLent()
    {
        $subQuery = new Query();
        $query = new Query();
        $allSubcriptions = $subQuery->from('subscription')->where('subscriber = :subscriber',['subscriber' => Yii::$app->user->identity->id])->select('subscription.author');
        $provider = new ArrayDataProvider([

            'allModels' => $query->from('articles')
                ->select('articles.id, title, text, users.id as author, users.username')
                ->where(['IN', 'articles.author', $subQuery])
                ->leftJoin('users','users.id = articles.author')
                ->all(),
            'sort' => [
                'attributes' => ['id', 'created_at'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $articles = $provider->getModels();

        return $this->render('lent', [
            'articles' => $articles,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $isMaster = $model->isMaster();
        $author = $this->findUser($model->author);
        $isSubsriber = $author->isSubsriber(Yii::$app->user->identity->id);

        return $this->render('view', [
            'model' => $model,
            'author' => $author,
            'isMaster' => $isMaster,
            'isSubsriber' => $isSubsriber,
        ]);
    }

    public function actionSubscription($id, $article){
        $subscription = new Subscription();
        $res = $subscription->setSubscription($id, Yii::$app->user->identity->id);
        if ($res){
            return $this->redirect(['view', 'id' => $article]);
        }else{
            return $this->redirect(['view', 'id' => $article]);
        }
    }

    public function actionUnsubscription($id, $article){
        $res = Subscription::deleteAll('author = :author AND subscriber = :subscriber',[':author' => $id, ':subscriber' => Yii::$app->user->identity->id]);
        if ($res){
            return $this->redirect(['view', 'id' => $article]);
        }else{
            return $this->redirect(['view', 'id' => $article]);
        }
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();

        $model->author = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['my']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findUser($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
