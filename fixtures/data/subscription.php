<?php
return [
    ['id' => 1, 'subscriber' => 1, 'author' => 2],
    ['id' => 2, 'subscriber' => 1, 'author' => 3],
    ['id' => 3, 'subscriber' => 1, 'author' => 4],

    ['id' => 4, 'subscriber' => 2, 'author' => 1],
    ['id' => 5, 'subscriber' => 2, 'author' => 4],

    ['id' => 6, 'subscriber' => 3, 'author' => 1],
    ['id' => 7, 'subscriber' => 3, 'author' => 2],
    ['id' => 8, 'subscriber' => 3, 'author' => 4],

    ['id' => 9, 'subscriber' => 4, 'author' => 1],
    ['id' => 10, 'subscriber' => 4, 'author' => 2],
];