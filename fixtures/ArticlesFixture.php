<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class ArticlesFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Articles';
    public $dataFile = '@app/fixtures/data/articles.php';
}