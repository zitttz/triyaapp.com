<?php

namespace app\fixtures;

use yii\test\ActiveFixture;

class SubscriptionFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Subscription';
    public $dataFile = '@app/fixtures/data/subscription.php';
}