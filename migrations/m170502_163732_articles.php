<?php

use yii\db\Migration;

class m170502_163732_articles extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'text' => $this->text()->notNull(),
            'author' => $this->integer()->notNull(),

            'created_at' => $this->dateTime() . ' DEFAULT NOW()',
            'updated_at' => $this->dateTime() . ' DEFAULT NOW()',
        ], $tableOptions);

        $this->createIndex(
            'idx_articles_author',
            'articles',
            'author'
        );

        $this->addForeignKey(
            'fk_articles_author',
            'articles',
            'author',
            'users',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_articles_author', 'articles');
        $this->dropIndex('idx_articles_author', 'articles');
        $this->dropTable('articles');
    }
}
