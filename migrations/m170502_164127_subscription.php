<?php

use yii\db\Migration;

class m170502_164127_subscription extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('subscription', [
            'id' => $this->primaryKey(),
            'subscriber' => $this->integer()->notNull(),
            'author' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx_subscription_subscriber',
            'subscription',
            'subscriber'
        );

        $this->createIndex(
            'idx_subscription_author',
            'subscription',
            'author'
        );

        $this->addForeignKey(
            'fk_subscription_subscriber',
            'subscription',
            'subscriber',
            'users',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_subscription_author',
            'subscription',
            'author',
            'users',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('idx_subscription_subscriber', 'subscription');
        $this->dropForeignKey('fk_subscription_author', 'subscription');
        $this->dropIndex('idx_subscription_author', 'subscription');
        $this->dropIndex('idx_subscription_subscriber', 'subscription');
        $this->dropTable('subscription');
    }
}
