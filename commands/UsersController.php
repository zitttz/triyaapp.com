<?php
namespace app\commands;
use yii\console\Controller;
use Yii;
use app\models\Users;

class UsersController extends Controller
{
    public function actionCreate()
    {

        foreach ($this->getArr() as $row) {
            $model = Users::find()->where(['username' => $row['username']])->one();
            if (empty($model)) {
                $user = new Users();
                $user->username = $row['username'];
                $user->email = $row['email'];
                $user->setPassword($row['password']);
                echo $user->password_hash;
                $user->generateAuthKey();
                if ($user->save()) {
                    echo 'create: ' . $row['username'] . PHP_EOL;
                } else {
                    echo 'error: ' . $row['username'] . PHP_EOL;
                    var_dump($user->getErrors());
                }
            }
        }
    }

    private function getArr()
    {
        return [
            ['id' => 1, 'username' => 'admin', 'email' => 'admin@mail.com', 'password' => 123],

            ['id' => 21, 'username' => 'rogov', 'email' => 'rogov@mail.com', 'password' => 123],

            ['id' => 2, 'username' => 'bessionov', 'email' => 'bessionov@mail.com', 'password' => 123],

            ['id' => 3, 'username' => 'ivanov', 'email' => 'ivanov@mail.com', 'password' => 123],

            ['id' => 4, 'username' => 'pereman', 'email' => 'pereman@mail.com', 'password' => 123],

            ['id' => 5, 'username' => 'petrov', 'email' => 'petrov@mail.com', 'password' => 123],

            ['id' => 6, 'username' => 'sidorov', 'email' => 'sidorov@mail.com', 'password' => 123],

            ['id' => 7, 'username' => 'galya002', 'email' => 'galya002@mail.com', 'password' => 123],

            ['id' => 8, 'username' => 'sveta_12', 'email' => 'sveta_12@mail.com', 'password' => 123],

            ['id' => 9, 'username' => 'bezzakonie', 'email' => 'bezzakonie@mail.com', 'password' => 123],

            ['id' => 10, 'username' => 'rostovpapa1985', 'email' => 'rostovpapa1985@mail.com', 'password' => 123],

            ['id' => 11, 'username' => 'uut', 'email' => 'uut@mail.com', 'password' => 123],
        ];
    }
}